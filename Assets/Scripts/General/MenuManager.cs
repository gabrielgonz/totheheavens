﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MenuManager : MonoBehaviour {

    public List<string> text;

    private List<Transform> platformInstance;
    private int currentCameraPos = 0;
    private Vector2 screenSize;

    enum StartMenu
    {
        Play,
        MoreGames,
        Options,
        Exit
        
    }

	// Use this for initialization
	void Start ()
    {
        screenSize = GetComponent<RectTransform>().sizeDelta;

        platformInstance = new List<Transform>();

        GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        cylinder.transform.localScale = new Vector3(50f, 100f, 50f);
        cylinder.transform.position = new Vector3(0f, 50f, 0f);

        platformInstance.Add(PlacePlatform(270f, 25f, 15f).transform);
        platformInstance.Add(PlacePlatform(0f, 25f, 15f).transform);
        platformInstance.Add(PlacePlatform(90f, 25f, 15f).transform);
        platformInstance.Add(PlacePlatform(180f, 25f, 15f).transform);

        StartCoroutine(PressToPlay());
        //placeButtons();
        PlaceButton(StartMenu.Play);
        PlaceButton(StartMenu.Exit);
        PlaceButton(StartMenu.Options);
        PlaceButton(StartMenu.MoreGames);
    }

    IEnumerator PressToPlay()
    {
        bool pressToPlay = true;

        //texto inferior "Pulsa para jugar"
        Text startText = new GameObject("PulsaComenzar").AddComponent<Text>();
        startText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        startText.text = "Pulsa Para Comenzar";
        startText.alignment = TextAnchor.MiddleCenter;
        startText.gameObject.AddComponent<Outline>();
        RectTransform startTextTransform = startText.GetComponent<RectTransform>();
        startTextTransform.anchoredPosition3D = new Vector3(0f, screenSize.y * 0.1f, 0f);
        startTextTransform.sizeDelta = new Vector2(screenSize.x, screenSize.y*0.2f);
        startTextTransform.anchorMax = new Vector2(0.5f, 0f);
        startTextTransform.anchorMin = new Vector2(0.5f, 0f);
        startTextTransform.SetParent(transform, false);

        //texto superior Titulo del juego
        Text titleText = new GameObject("ToTheHeavens").AddComponent<Text>();
        titleText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        titleText.text = "To The Heavens";
        titleText.alignment = TextAnchor.MiddleCenter;
        titleText.gameObject.AddComponent<Outline>();
        RectTransform titleTextTransform = titleText.GetComponent<RectTransform>();
        titleTextTransform.anchoredPosition3D = new Vector3(0f, screenSize.y * 0.6f, 0f);
        titleTextTransform.sizeDelta = new Vector2(screenSize.x, screenSize.y * 0.8f);
        titleTextTransform.anchorMax = new Vector2(0.5f, 0f);
        titleTextTransform.anchorMin = new Vector2(0.5f, 0f);
        titleTextTransform.SetParent(transform, false);

        while (pressToPlay)
        {
            if (Input.GetMouseButtonDown(0))
                pressToPlay = false;

            yield return 0;
        }
        startText.gameObject.SetActive(false);
        titleText.gameObject.SetActive(false);
        StartCoroutine(MoveTowardsButton());
    }

    IEnumerator MoveTowardsButton()
    {
        float lerpTime = 1f;
        float currentLerpTime = 0f;
        bool lerping = true;
        Transform cameraTransform = Camera.main.transform;
        Vector3 startPos = cameraTransform.position;
        Vector3 endPos = platformInstance[currentCameraPos].position;
        endPos.y += 50f;
        Quaternion startRotation = cameraTransform.rotation;
        Quaternion endRotation = Quaternion.Euler(90f, 0f, 0f);
        while(lerping)
        {

            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
                lerping = false;
            }
            cameraTransform.rotation = Quaternion.Lerp(startRotation, endRotation, currentLerpTime);
            cameraTransform.position = Vector3.Lerp(startPos, endPos, currentLerpTime);
            yield return 0;
        }
        StartCoroutine(KeyPress());
    }

    IEnumerator KeyPress()
    {
        bool checkKeyPress = true;
        Transform cameraTransform = Camera.main.transform;
        while(checkKeyPress)
        {
            if(Input.GetKey(KeyCode.LeftArrow))
                cameraTransform.RotateAround(Vector3.zero, Vector3.down, 50 * Time.deltaTime);
            if (Input.GetKey(KeyCode.RightArrow))
                cameraTransform.RotateAround(Vector3.zero, Vector3.down, -50 * Time.deltaTime);

            yield return 0;
        }
    }

    GameObject PlacePlatform(float angle, float cylinderRadius, float platformRadius)
    {
        float radius = cylinderRadius + platformRadius;

        float x = radius * Mathf.Cos(angle * Mathf.Deg2Rad);
        float y = radius * Mathf.Sin(angle * Mathf.Deg2Rad);

        GameObject primitive = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        primitive.transform.position = new Vector3(x, 0f, y);
        primitive.transform.localScale = new Vector3(platformRadius * 2f, 1f, platformRadius * 2f);
        Destroy(primitive.GetComponent<CapsuleCollider>());
        primitive.AddComponent<BoxCollider>();

        return primitive;
    }

    void PlaceButton(StartMenu button)
    {
        Text textComponent = new GameObject("Texto").AddComponent<Text>();
        RectTransform textTransform = textComponent.GetComponent<RectTransform>();
        //image.anchorMax = Vector2.zero;
        //image.anchorMin = Vector2.zero;
        Vector3 pos = platformInstance[(int)button].position;
        pos.y += 1.1f;
        textTransform.anchoredPosition3D = pos;
        textTransform.rotation = Quaternion.Euler(90f, 90f * (text.Count - (int)button), 0f);
        textTransform.SetParent(transform);
        textTransform.localScale = Vector3.one;
        textTransform.sizeDelta = new Vector2(150f, 150f);
        textComponent.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        textComponent.text = text[(int)button];
        textComponent.color = Color.black;
        textComponent.alignment = TextAnchor.MiddleCenter;
        textComponent.fontSize = 45;
        textComponent.resizeTextForBestFit = true;

        EventTrigger onClic = textTransform.gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry lEntry = new EventTrigger.Entry();
        lEntry.eventID = EventTriggerType.PointerClick;
        //lEntry.callback.AddListener((eventData) => loadLevel());
        //onClic.triggers.Add(lEntry);
        switch (button)
        {
            case StartMenu.Play:
                lEntry.callback.AddListener((eventData) => LoadLevel());
                onClic.triggers.Add(lEntry);
                break;
            case StartMenu.MoreGames:
                lEntry.callback.AddListener((eventData) => MoreGames());
                onClic.triggers.Add(lEntry);
                break;
            case StartMenu.Options:
                lEntry.callback.AddListener((eventData) => Options());
                onClic.triggers.Add(lEntry);
                break;
            case StartMenu.Exit:
                lEntry.callback.AddListener((eventData) => Exit());
                onClic.triggers.Add(lEntry);
                break;
        }
    }

    void PlaceButtons()
    {
        int counter = 4;
        foreach (Transform item in platformInstance)
        {
            Debug.Log(counter);
            Text textComponent = new GameObject("Texto").AddComponent<Text>();
            RectTransform textTransform = textComponent.GetComponent<RectTransform>();
            //image.anchorMax = Vector2.zero;
            //image.anchorMin = Vector2.zero;
            Vector3 pos = item.position;
            pos.y += 1.1f;
            textTransform.anchoredPosition3D = pos;
            textTransform.rotation = Quaternion.Euler(90f, 90f*counter, 0f);
            textTransform.SetParent(transform);
            textTransform.localScale = Vector3.one;
            textComponent.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            textComponent.text = "Play";
            textComponent.color = Color.black;
            textComponent.alignment = TextAnchor.MiddleCenter;
            textComponent.fontSize = 45;
            counter--;

            EventTrigger onClic = textTransform.gameObject.AddComponent<EventTrigger>();
            EventTrigger.Entry lEntry = new EventTrigger.Entry();
            lEntry.eventID = EventTriggerType.PointerClick;
            lEntry.callback.AddListener((eventData) => LoadLevel());
            onClic.triggers.Add(lEntry);
        }
    }

    void LoadLevel()
    {
        SceneManager.LoadScene("mainScene");
    }

    void Options()
    {
        Debug.Log("Menu Opciones");
    }

    void MoreGames()
    {
        Debug.Log("Menu Mas Juegos");
    }

    void Exit()
    {
        Application.Quit();
    }
}
