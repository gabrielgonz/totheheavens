﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class CreateUI : MonoBehaviour {

    public ThirdPersonUserControl player;
    private Vector2 screenSize;

    //Crea los botones de la UI para caminar y saltar en caso de estar en un dispositivo movil
#if UNITY_ANDROID
	void Start ()
    {
        screenSize = GetComponent<RectTransform>().sizeDelta;
        //creacion de los botones
        RectTransform leftButton = new GameObject().AddComponent<RectTransform>();
        leftButton.transform.SetParent(transform, false);
        leftButton.gameObject.AddComponent<Image>().color = Color.red;
        leftButton.sizeDelta = Vector2.one * screenSize.y * 0.2f;
        leftButton.anchoredPosition3D = new Vector3(screenSize.x / -4f, screenSize.y / -4f);

        RectTransform rightButton = new GameObject().AddComponent<RectTransform>();
        rightButton.transform.SetParent(transform, false);
        rightButton.gameObject.AddComponent<Image>().color = Color.red;
        rightButton.sizeDelta = Vector2.one * screenSize.y * 0.2f;
        rightButton.anchoredPosition3D = new Vector3(screenSize.x / 4f, screenSize.y / -4f);

        //creacion del detector de eventos
        EventTrigger leftTrigger = leftButton.gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry lEntry = new EventTrigger.Entry();
        lEntry.eventID = EventTriggerType.PointerDown;
        lEntry.callback.AddListener((eventData) => player.StartWalk());
        leftTrigger.triggers.Add(lEntry);
        lEntry = new EventTrigger.Entry();
        lEntry.eventID = EventTriggerType.PointerUp;
        lEntry.callback.AddListener((eventData) => player.EndWalk());
        leftTrigger.triggers.Add(lEntry);

        EventTrigger rightTrigger = rightButton.gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry rEntry = new EventTrigger.Entry();
        rEntry.eventID = EventTriggerType.PointerClick;
        rEntry.callback.AddListener((eventData) => { player.jump(); });
        rightTrigger.triggers.Add(rEntry);
    }
#endif
}
