using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody))]
public class ThirdPersonUserControl : MonoBehaviour
{
    public LayerMask layerMask;
    private Rigidbody m_Rigidbody;
    private Animator m_Animator;
    private bool walking = false;
    private bool onGround = true;

    private float lerpTime = 5f;
    private float currentLerpTime = 0f;

    public void StartWalk()
    {
        walking = true;
        m_Rigidbody.constraints = RigidbodyConstraints.None;
        m_Rigidbody.freezeRotation = false;
        
    }

    public void EndWalk()
    {
        Vector3 currentRot = transform.rotation.eulerAngles;
        currentRot.x = 0f;
        currentRot.z = 0f;
        transform.rotation = Quaternion.Euler(currentRot);

        walking = false;
        m_Rigidbody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        m_Rigidbody.freezeRotation = true;

    }

    private void Start()
    {
        // get the third person character ( this should never be null due to require component )
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Rigidbody.freezeRotation = true;

        StartCoroutine(PlayerMovement());

    }

    void CheckGroundStatus()
    {
        RaycastHit hitInfo;
#if UNITY_EDITOR
        // helper to visualise the ground check ray in the scene view
        Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * 2f),Color.red);
#endif
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 1f), Vector3.down, out hitInfo, 2.5f,layerMask))
        {
            onGround = true;
            m_Animator.applyRootMotion = true;
            m_Animator.SetBool("OnGround", true);
        }
        else
        {
            onGround = false;
            m_Animator.applyRootMotion = false;
            m_Animator.SetBool("OnGround", false);
        }
    }

    public void Jump()
    {
        if (onGround == true)
        {
            m_Rigidbody.AddForce(Vector3.up * 1200, ForceMode.Impulse);
            onGround = false;
        }
    }

    private void FixedUpdate()
    {
        CheckGroundStatus();
    }

    IEnumerator PlayerMovement()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.D))
                Jump();

            if (Input.GetKeyDown(KeyCode.A))
                StartWalk();

            if (Input.GetKeyUp(KeyCode.A))
                EndWalk();

            yield return 0;
        }
    }

    private void Update()
    {

        if (!onGround)
        {
            m_Animator.SetFloat("Jump", m_Rigidbody.velocity.y);
        }

        if (walking)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
                currentLerpTime = 0f;

            float perc = currentLerpTime / lerpTime;
            float angle = Mathf.Lerp(0f, 360f, perc);

            transform.position = new Vector3(40f * Mathf.Cos(angle * Mathf.Deg2Rad), transform.position.y, 40f * Mathf.Sin(angle * Mathf.Deg2Rad));
            transform.rotation = Quaternion.Euler(0f, angle * -1f, 0f);

            m_Animator.SetFloat("Forward", 1f, 0.1f, Time.deltaTime);
        }else
        {
            m_Animator.SetFloat("Forward", 0f, 0.1f, Time.deltaTime);
        }
    }
}
