﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour
{
    private bool walking = false;
    private bool collided = false;
    private bool onGround = true;
    private Rigidbody charRigidBody;
    private Animator animationInstance;

    private float lerpTime = 5f;
    private float currentLerpTime = 0f;

    void OnCollisionEnter(Collision collision)
    {
        collided = true;
        foreach (ContactPoint contact in collision.contacts)
        {
            float point = Mathf.Round((transform.position.y - contact.point.y));
            if (point == 0)
            {
                onGround = true;
                animationInstance.SetBool("Jumping", false);
            }
        }
    }

    public void startWalk()
    {
        walking = true;
        charRigidBody.freezeRotation = false;
        animationInstance.SetBool("Running", true);
    }

    public void endWalk()
    {
        walking = false;
        charRigidBody.freezeRotation = true;
        animationInstance.SetBool("Running", false);
    }

    public void jump()
    {
        if (onGround == true)
        {
            charRigidBody.AddForce(Vector3.up * 1200, ForceMode.Impulse);
            onGround = false;
            collided = false;
            animationInstance.SetBool("Jumping", true);
        }
    }

    void Start()
    {
        charRigidBody = GetComponent<Rigidbody>();
        animationInstance = GetComponent<Animator>();

        charRigidBody.freezeRotation = true;
    }

    void Update()
    {
        if (walking && collided && onGround || walking && !collided)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
                currentLerpTime = 0f;

            float perc = currentLerpTime / lerpTime;
            float angle = Mathf.Lerp(0f, 360f, perc);

            transform.position = new Vector3(40f*Mathf.Cos(angle*Mathf.Deg2Rad), transform.position.y, 40f * Mathf.Sin(angle * Mathf.Deg2Rad));
            transform.rotation = Quaternion.Euler(0f, angle*-1f, 0f);

            /*transform.RotateAround(Vector3.zero, Vector3.down, 50 * Time.deltaTime);
            Vector3 fixedPos = new Vector3(Mathf.Clamp(transform.position.x, -40f, 40f), transform.position.y, Mathf.Clamp(transform.position.z, -40f, 40f));
            transform.position = fixedPos;

            float yRotation = Mathf.Asin((transform.position.z * -1f / 40.1f)) * Mathf.Rad2Deg;

            if (transform.position.x < 0)
                yRotation = 180f - yRotation;
 
            transform.rotation = Quaternion.Euler(0f,yRotation , 0f);
            //Debug.Log(Mathf.Asin((transform.position.z / 40f))*Mathf.Rad2Deg);*/
        }
    }
}
