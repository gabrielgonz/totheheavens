﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

    public Transform target;
    private float yHeight = 0;
    private BoxCollider bottomCollider;

    void Start()
    {
        bottomCollider = new GameObject().AddComponent<BoxCollider>();
        bottomCollider.size = new Vector3(100f, 0.5f, 100f);
        bottomCollider.transform.position = new Vector3(0f,target.transform.position.y -40f, 0f);
        bottomCollider.isTrigger = true;
        bottomCollider.gameObject.AddComponent<ReloadLevel>();
    }

	void Update ()
    {
        Vector3 targetRotation = target.rotation.eulerAngles;
        transform.rotation = Quaternion.Euler(0f,targetRotation.y-90f, 0f);

        float targetY = target.position.y;
        if (targetY > yHeight)
            yHeight = target.position.y;

        Vector3 pos = new Vector3(125 * Mathf.Cos(targetRotation.y * Mathf.Deg2Rad), 0f, -125 * Mathf.Sin(targetRotation.y * Mathf.Deg2Rad))
        {
            y = Mathf.Clamp(targetY, yHeight - 10f, yHeight)
        };

        transform.position = pos;
        bottomCollider.transform.position = new Vector3(0f, transform.position.y - 40f, 0f);
    }
}
