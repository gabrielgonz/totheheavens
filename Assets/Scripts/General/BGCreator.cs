﻿using UnityEngine;
using System.Collections;

public class BGCreator : MonoBehaviour {

    public Transform hillGameobject;
    public float hillNumber = 20f;
    public float distanceFromCenter = 50f;

    private Transform hillInstance;

	// Use this for initialization
	void Start ()
    {
        for(float i = 0;i< hillNumber; i++)
        {
            float angle = 360f / hillNumber;
            hillInstance = Instantiate(hillGameobject).transform;
            hillInstance.position = new Vector3(distanceFromCenter * Mathf.Sin(angle * i * Mathf.Deg2Rad), distanceFromCenter*0.2f, distanceFromCenter * Mathf.Cos(angle * i * Mathf.Deg2Rad));
            hillInstance.LookAt(Vector3.zero);
            Transform childTransform = hillInstance.GetChild(0).transform;
            float numb = distanceFromCenter * 0.9f;
            childTransform.localScale = new Vector3(numb * 1.5f, numb, 1f);

            hillInstance = Instantiate(hillGameobject).transform;
            hillInstance.position = new Vector3(distanceFromCenter * 0.8f * Mathf.Sin(((angle*i)+(angle/2f))*Mathf.Deg2Rad), distanceFromCenter*0.8f*0.2f, distanceFromCenter * 0.8f * Mathf.Cos(((angle * i) + (angle / 2f)) * Mathf.Deg2Rad));
            hillInstance.LookAt(Vector3.zero);
            childTransform = hillInstance.GetChild(0).transform;
            numb = distanceFromCenter * 0.8f * 0.9f;
            childTransform.localScale = new Vector3(numb * 1.5f, numb, 1f);

        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
