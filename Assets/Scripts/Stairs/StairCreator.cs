﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class StairCreator : MonoBehaviour {

    public GameObject stairPrefab;
    public GameObject towerPrefab;
    public GameObject coinPrefab;
    public Transform playerInstance;


    private int stairCounter = 0;
    private int towerCounter = 0;
    private Transform stairContainer;
    private List<Transform> unusedGameobjects;
    private List<Transform> usedGameobjects;

    private List<Transform> unusedTowers;
    private List<Transform> usedTowers;

    private List<Transform> unusedCoins;
    private List<Transform> usedCoins;

    // Use this for initialization
    void Start ()
    {
        unusedGameobjects = new List<Transform>();
        usedGameobjects = new List<Transform>();
        unusedTowers = new List<Transform>();
        unusedCoins = new List<Transform>();
        usedTowers = new List<Transform>();
        usedCoins = new List<Transform>();

        FillPool();

        LoadPlatforms();
        PlaceStairs();
        PlaceStairs();
        PlaceStairs();
        CreateTower();
    }

    void FillPool()
    {
        /*for (int i = 0; i < 40; i++)
        {
            Transform stair = Instantiate(stairPrefab).transform;
            stair.position = Vector3.down * 100f;
            unusedGameobjects.Add(stair);
        }*/

        for (int i = 0; i < 25; i++)
        {
            Transform coin = Instantiate(coinPrefab).transform;
            coin.position = Vector3.down * 100f;
            unusedCoins.Add(coin);
        }

        for (int i = 0; i < 3; i++)
        {
            Transform tower = Instantiate(towerPrefab).transform;
            tower.position = Vector3.down * 100f;
            unusedTowers.Add(tower);
        }
    }


    Transform GetCoin()
    {
        int coinCount = unusedCoins.Count;
        Transform coinInstance=null;
        if (coinCount != 0)
        {
            coinInstance = unusedCoins[coinCount - 1];
            unusedCoins.RemoveAt(coinCount - 1);
            usedCoins.Add(coinInstance);
        }

        return coinInstance;
    }

    IEnumerator CoinCheck(Transform coinTransform)
    {
        CoinScriptStairs script = coinTransform.gameObject.GetComponent<CoinScriptStairs>();

        bool running = true;
        float changePos = coinTransform.position.y + 50f;
        while (running)
        {
            if(playerInstance.position.y>changePos || script.touched)
            {
                running = false;
            }

            yield return 0;
        }

        for (int i = 0; i < usedCoins.Count; i++)
        {
            if(coinTransform == usedCoins[i])
            {
                if(i!=usedCoins.Count-1)
                {
                    usedCoins[i] = usedCoins[usedCoins.Count - 1];
                    usedCoins[usedCoins.Count - 1] = coinTransform;
                }

                usedCoins.RemoveAt(usedCoins.Count - 1);

                break;
            }
        }
        script.touched = false;
        coinTransform.position = Vector3.down * 100f;
        unusedCoins.Add(coinTransform);
    }

    Transform GetTower()
    {
        int towerCount = unusedTowers.Count;
        Transform towerInstance = null;
        if (towerCount != 0)
        {
            towerInstance = unusedTowers[towerCount - 1];
            unusedTowers.RemoveAt(towerCount - 1);
        }

        usedTowers.Add(towerInstance);
        return towerInstance;
    }

    IEnumerator TowerCheck(Transform towerTransform)
    {
        bool running = true;
        float changePos = towerTransform.position.y;
        while (running)
        {
            if (playerInstance.position.y > changePos)
                running = false;

            yield return 0;
        }

        for (int i = 0; i < usedTowers.Count; i++)
        {
            if(towerTransform == usedTowers[i])
            {
                if(i != usedTowers.Count-1)
                {
                    usedTowers[i] = usedTowers[usedTowers.Count - 1];
                    usedTowers[usedTowers.Count - 1] = towerTransform;
                }

                usedTowers.RemoveAt(usedTowers.Count - 1);

                break;
            }
        }
        CreateTower();
        unusedTowers.Add(towerTransform);

    }

    void CreateTower()
    {
        float towerHeight = 200f;
        Vector3 towerPos = Vector3.zero;
        towerPos.y = 90f + towerHeight * towerCounter;
        Transform towerInstance = GetTower();
        towerInstance.position = towerPos;
        StartCoroutine(TowerCheck(towerInstance));
        towerCounter++;
    }

    void LoadPlatforms()
    {
        Transform stairInstance;
        stairContainer = new GameObject("Contenedor").transform;
        for(float i = 0;i<80;i++)
        {
            stairInstance = Instantiate(stairPrefab).transform;
            stairInstance.SetParent(stairContainer);
            stairInstance.gameObject.SetActive(false);
            unusedGameobjects.Add(stairInstance);
        }
    }

    void PlaceStairs()
    {
        float deg = 14.4f;
        Transform stairInstance;

        for (float i = 0; i < 25; i++)
        {
            stairInstance = unusedGameobjects[0].transform;
            unusedGameobjects.RemoveAt(0);
            usedGameobjects.Add(stairInstance);
            usedGameobjects[usedGameobjects.Count - 1].gameObject.SetActive(true);
            stairInstance.position = new Vector3(0f, 2f * stairCounter, 0f);
            stairInstance.rotation = Quaternion.Euler(0f, deg * i * -1f, 0f);
            stairCounter++;
            if(i == 12)
            {
                for(int j=0;j<5;j++)
                {
                    Transform coin = GetCoin();
                    Vector3 posi = stairInstance.position;
                    posi.x = Mathf.Sin(4f*j * Mathf.Deg2Rad) * 40f;
                    posi.z = Mathf.Cos(4f*j * Mathf.Deg2Rad) * 40f;
                    posi.y -= (50f/360f)*j*4f;
                    coin.position = posi;
                    StartCoroutine(CoinCheck(coin));
                }

            }
        }

    }

	// Update is called once per frame
	void Update ()
    {
        if ((playerInstance.position.y - usedGameobjects[0].position.y) > 50f)
        {
            usedGameobjects[0].gameObject.SetActive(false);
            unusedGameobjects.Add(usedGameobjects[0]);
            usedGameobjects.RemoveAt(0);
            if (unusedGameobjects.Count >= 25)
                PlaceStairs();
        }
	}
}
