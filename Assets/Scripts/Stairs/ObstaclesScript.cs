﻿using UnityEngine;
using System.Collections;

public class ObstaclesScript : MonoBehaviour {

    public GameObject obstacleInstance;
    public Transform targetPlayer;
    private float time;
	
	// Update is called once per frame
	void Update ()
    {
        time += Time.deltaTime;
        if(time >2f)
        {
            time = 0f;
            BarrelScript barrel = (Instantiate(obstacleInstance, targetPlayer.position * -1f, Quaternion.identity)as GameObject).GetComponent<BarrelScript>();
            barrel.startingAngle = targetPlayer.transform.rotation.eulerAngles.y;
            barrel.startingHeight = targetPlayer.transform.position.y;
        }
	}
}
