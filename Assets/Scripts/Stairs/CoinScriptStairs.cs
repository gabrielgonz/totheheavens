﻿using UnityEngine;
using System.Collections;

public class CoinScriptStairs : MonoBehaviour {

    public bool touched = false;
    private float lerpTime;
    private float currentTime;
    private AudioSource audioInstance;
    private Rigidbody rigidbodyInstance;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            audioInstance.Play();
            touched = true;
        }
    }

    void Start()
    {
        audioInstance = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update ()
    {
        transform.RotateAround(transform.position, Vector3.up, 90 * Time.deltaTime);
    }
}
