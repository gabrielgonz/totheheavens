﻿using UnityEngine;
using System.Collections;

public class BarrelScript : MonoBehaviour {

    public float startingAngle = 180f;
    public float startingHeight = 0f;
    private float lerpTime;
    private float currentTime;

	// Use this for initialization
	void Start ()
    {
        lerpTime = 5f;
        currentTime = 0f;
        Vector3 pos = transform.position;
        pos.y = startingHeight + 29f;
        transform.position = pos;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if ((Camera.main.transform.position.y - transform.position.y) > 50f)
            Destroy(gameObject);

        currentTime += Time.deltaTime;
        if (currentTime > lerpTime)
            currentTime = 0f;

        float perc = currentTime / lerpTime;
        float angle = Mathf.Lerp(360f, 0f, perc);
        angle -= 180f;
        angle -= startingAngle;
        transform.position = new Vector3(40f * Mathf.Cos(angle * Mathf.Deg2Rad), transform.position.y, 40f * Mathf.Sin(angle * Mathf.Deg2Rad));
        transform.rotation = Quaternion.Euler(0f, angle * -1f, 0f);
    }
}
