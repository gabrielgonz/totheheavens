﻿using UnityEngine;
using System.Collections;

public class CoinScriptPlatform : MonoBehaviour {

    private AudioSource audioInstance;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            audioInstance.Play();
            Destroy(gameObject,audioInstance.clip.length);
        }
    }

    void Start()
    {
        audioInstance = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update ()
    {
        transform.RotateAround(transform.position, Vector3.up, 100 * Time.deltaTime);
	}
}
