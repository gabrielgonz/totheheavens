﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {

    public Transform player;
    public GameObject platformPrefab;
    public GameObject coinPrefab;

    private List<GameObject> usedPlatforms;
    private List<GameObject> unusedPlatforms;
    private int platformCounter = 0;
    private int removedCounter = 0;
    private Transform centerCylinder;

    private float heightCounter;
    private float platformHeight = 7f;

    private PlatformScript.Types previousType = PlatformScript.Types.Elevador;

    // Use this for initialization
    void Start ()
    {
        usedPlatforms = new List<GameObject>();
        unusedPlatforms = new List<GameObject>();

        heightCounter = player.transform.position.y + platformHeight*6f;

        centerCylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder).transform;

        placePlatforms(6f);
        placePlatforms(6f);
        placePlatforms(6f);
        placePlatforms(6f);

        updateCylinder();
    }

    void Update()
    {
        if ((player.transform.position.y - usedPlatforms[0].transform.position.y) > 50f)
        {
            unusedPlatforms.Add(usedPlatforms[0]);
            unusedPlatforms[unusedPlatforms.Count - 1].SetActive(false);
            usedPlatforms.RemoveAt(0);
            heightCounter += platformHeight;
            removedCounter++;
        }

        if(removedCounter== 6)
        {
            placePlatforms(6f);
            updateCylinder();
            removedCounter = 0;
        }
    }

    GameObject returnUnusedPlatform()
    {
        if (unusedPlatforms.Count == 0)
        {
            for (int i = 0; i < 30; i++)
            {
                unusedPlatforms.Add(Instantiate(platformPrefab) as GameObject);
                unusedPlatforms[i].name = "Plataforma";
                unusedPlatforms[i].SetActive(false);
            }
        }
        usedPlatforms.Add(unusedPlatforms[0]);
        unusedPlatforms.RemoveAt(0);
        return usedPlatforms[usedPlatforms.Count - 1];
    }

    GameObject createPlatform(float angle, float cylinderRadius, float platformRadius)
    {
        float radius = cylinderRadius + platformRadius;

        float x = radius * Mathf.Cos(angle * Mathf.Deg2Rad);
        float y = radius * Mathf.Sin(angle * Mathf.Deg2Rad);

        GameObject primitive = returnUnusedPlatform();
        primitive.SetActive(true);
        primitive.transform.position = new Vector3(x, platformCounter * platformHeight, y);
        primitive.transform.localScale = new Vector3(platformRadius * 2f, 1f, platformRadius * 2f);
        PlatformScript pScript = primitive.AddComponent<PlatformScript>();

        if (previousType != PlatformScript.Types.Normal)
        {
            pScript.tipoDePlataforma = PlatformScript.Types.Normal;
            previousType = pScript.tipoDePlataforma;
        }
        else
        {
            int numero = Random.Range(0, 4);
            pScript.tipoDePlataforma = (PlatformScript.Types)numero;
            previousType = pScript.tipoDePlataforma;
        }

        pScript.tipoDePlataforma = PlatformScript.Types.Normal;

        if (pScript.tipoDePlataforma == PlatformScript.Types.Elevador)
        {
            Vector3 pos = primitive.transform.position;
            pos.y += 20f;
            primitive.transform.position = pos;
            platformCounter += 3;
        }


        primitive.layer = 8;
        platformCounter++;
        primitive.SetActive(false);
        primitive.SetActive(true);
        return primitive;
    }

    void placePlatforms(float number)
    {
        float angle = 360f / number;
        for (float i = 0; i < number; i++)
        {
            GameObject platform = createPlatform(angle * i, 25f, 10f);
            Vector3 pos = platform.transform.position;
            pos.y += 20;
            Instantiate(coinPrefab, pos, Quaternion.identity);
        }
    }

    void updateCylinder()
    {
        centerCylinder.transform.localScale = new Vector3(50f, ((platformCounter - 1f) * 5f) / 2f, 50f);
        centerCylinder.transform.position = new Vector3(0f, ((platformCounter - 1f) * 5f) / 2f, 0f);
    }
}
