﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlatformScript : MonoBehaviour {

    public enum Types
    {
        Normal,
        Elevador,
        Fantasma,
        Rotador
    }
    public Types tipoDePlataforma = Types.Normal;

    private BoxCollider boxCollider;

    void OnCollisionEnter(Collision collision)
    {
        if (collision != null && collision.gameObject.tag == "Player" && tipoDePlataforma == Types.Elevador)
            collision.transform.SetParent(transform);

        if (collision != null && collision.gameObject.tag == "Player" && tipoDePlataforma == Types.Fantasma)
            StartCoroutine(temblar());
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision != null && collision.gameObject.tag == "Player" && tipoDePlataforma == Types.Elevador)
            collision.transform.parent = null;
    }

    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    void OnEnable()
    {
        transform.LookAt(Vector3.zero);
        Vector3 rotat = transform.rotation.eulerAngles;
        rotat.x = rotat.z = 0f;
        transform.rotation = Quaternion.Euler(rotat);
        
        switch (tipoDePlataforma)
        {
            case Types.Normal:
                break;
            case Types.Elevador:
                StartCoroutine(ElevarPlataforma());
                break;
            case Types.Fantasma:
                //StartCoroutine(temblar());
                break;
            case Types.Rotador:
                StartCoroutine(rotar());
                break;
            default:
                break;
        }
    }

    IEnumerator rotar()
    {
        float time, currentTime;
        time = 0.5f;
        currentTime = 0f;
        /*Quaternion startRotation = Quaternion.Euler(new Vector3(0f,transform.rotation.eulerAngles.y,0f));
        Vector3 newRotation = startRotation.eulerAngles;
        newRotation.z = -90f;
        Quaternion endRotation = Quaternion.Euler(newRotation);*/
        Vector3 startRotation = transform.rotation.eulerAngles;
        Vector3 endRotation = startRotation;
        endRotation.z -= 90f;
        while (true)
        {
            currentTime += Time.deltaTime;

            if (currentTime > time)
            {
                currentTime = 0f;
                /*Quaternion saveRot = startRotation;
                startRotation = endRotation;
                endRotation = saveRot;*/
                Vector3 saveRot = startRotation;
                startRotation = endRotation;
                endRotation = saveRot;
                yield return new WaitForSeconds(1f);
            }

            float perc = currentTime / time;
            //transform.rotation = Quaternion.Lerp(startRotation, endRotation, perc);

            transform.rotation = Quaternion.Euler(Vector3.Lerp(startRotation, endRotation, perc));
            yield return 0;
        }
    }

    IEnumerator desaparecer()
    {
        float time, currentTime;
        time = 3f;
        currentTime = 0f;
        MeshRenderer renderer = GetComponent<MeshRenderer>();
        while (true)
        {
            currentTime += Time.deltaTime;

            if(currentTime > time)
            {
                currentTime = 0f;
                boxCollider.enabled = !boxCollider.enabled;
                renderer.enabled = !renderer.enabled;
            }

            yield return 0;
        }
    }

    IEnumerator temblar()
    {
        bool temblor = true;

        Transform modelTransform = transform.GetChild(0).transform;

        float yRotation = transform.rotation.eulerAngles.y*Mathf.Deg2Rad;

        float time, currentTime;
        time = 0.1f;
        currentTime = 0f;
        Vector3 startPos = transform.position;
        List<Vector3> endPos = new List<Vector3>();
        for(int i = 0;i<3; i++)
        {
            endPos.Add(startPos + new Vector3(1f*Mathf.Cos(yRotation),0f,-1f*Mathf.Sin(yRotation)));
            endPos.Add(startPos + new Vector3(-1f * Mathf.Cos(yRotation), 0f, 1f * Mathf.Sin(yRotation)));
        }

        int counter = 0;
        while (temblor)
        {
            currentTime += Time.deltaTime;

            if (currentTime > time)
            {
                currentTime = 0f;

                startPos = endPos[counter];
                counter++;

                if (counter >4)
                    temblor = false;
            }

            float perc = currentTime / time;
            modelTransform.position = Vector3.Lerp(startPos, endPos[counter], perc);

            yield return 0;
        }
        StartCoroutine(caer());
    }

    IEnumerator caer()
    {
        bool caida = true;

        float time, currentTime;
        time = 1f;
        currentTime = 0f;
        Vector3 startPos = transform.position;
        Vector3 endPos = transform.position;
        endPos.y -= 50f;

        while (caida)
        {
            currentTime += Time.deltaTime;

            if (currentTime > time)
            {
                currentTime = time;
            }

            float perc = currentTime / time;
            transform.position = Vector3.Lerp(startPos, endPos, perc);

            yield return 0;
        }
    }

    IEnumerator ElevarPlataforma()
    {
        bool elevar = true;
        Vector3 startPos,endPos;
        float currentTime, time;

        startPos = endPos = transform.position;
        endPos.y -= 20f;

        currentTime = 0f;
        time = 1f;

        while(elevar)
        {
            currentTime += Time.deltaTime;
            if(currentTime>time)
            {
                currentTime = 0f;
                Vector3 pos = endPos;
                endPos = startPos;
                startPos = pos;
                yield return new WaitForSeconds(0.2f);
            }

            float perc = currentTime / time;
            transform.position = Vector3.Lerp(startPos, endPos, perc);

            yield return 0;
        }
    }

}
