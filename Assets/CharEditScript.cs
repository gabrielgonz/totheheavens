﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class CharEditScript : MonoBehaviour {

    public GameObject buttonPrefab;
    private Vector2 screenSize;
    private List<Color> topObjList;
    private List<Color> midObjList;
    private List<Color> botObjList;
    private int topPos = 0;
    private int midPos = 0;
    private int botPos = 0;
    private RectTransform topImgDisp;
    private RectTransform midImgDisp;
    private RectTransform botImgDisp;

    // Use this for initialization
    void Start ()
    {
        screenSize = GetComponent<RectTransform>().sizeDelta;
        createMenu();

        topObjList = new List<Color>();
        midObjList = new List<Color>();
        botObjList = new List<Color>();

        topObjList.Add(Color.red);
        topObjList.Add(Color.black);
        topObjList.Add(Color.yellow);

        midObjList.Add(Color.blue);
        midObjList.Add(Color.green);

        botObjList.Add(Color.gray);
        botObjList.Add(Color.magenta);
        botObjList.Add(Color.cyan);
        botObjList.Add(Color.white);

    }
	
    void createMenu()
    {
        //creamos el fondo
        RectTransform bg = new GameObject("Background").AddComponent<Image>().GetComponent<RectTransform>();
        bg.SetParent(transform, false);
        bg.sizeDelta = new Vector2(screenSize.x / 2f, screenSize.y);
        Vector3 pos = bg.anchoredPosition3D;
        pos.x = screenSize.x / 4f;
        bg.anchoredPosition3D = pos;

        UnityAction[] leftButton = { null,() => StartCoroutine(chooseTopObject(0)), () => StartCoroutine(chooseMidObject(0)), () => StartCoroutine(chooseBotObject(0)) };
        UnityAction[] rightButton = { null, () => StartCoroutine(chooseTopObject(1)), () => StartCoroutine(chooseMidObject(1)), () => StartCoroutine(chooseBotObject(1)) };
        for (int i = 1; i <= 3; i++)
        {
            Image img = new GameObject("ScrollBG " + i.ToString()).AddComponent<Image>();
            img.color = Color.red;
            RectTransform scrollBG = img.GetComponent<RectTransform>();
            scrollBG.SetParent(transform, false);

            scrollBG.sizeDelta = new Vector2(screenSize.x / 2f,screenSize.y/6f);

            scrollBG.anchorMin = new Vector2(0.5f, 0f);
            scrollBG.anchorMax = scrollBG.anchorMin;

            Vector3 scrollBgPos = Vector3.zero;
            scrollBgPos.x = screenSize.x / 4f;
            scrollBgPos.y = screenSize.y - (screenSize.y / 4f) * i;

            scrollBG.anchoredPosition3D = scrollBgPos;

            //componentes de los botones izquierda derecha
            Vector3 buttonApos = new Vector3(0f + 50f, screenSize.y - (screenSize.y / 4f) * i, 0f);
            Vector3 buttonBpos = new Vector3((screenSize.x / 2f) - 50f, screenSize.y - (screenSize.y / 4f) * i, 0f);

            Button buttonAInstance = Instantiate(buttonPrefab).GetComponent<Button>();
            RectTransform scrollBgButtonA = buttonAInstance.GetComponent<RectTransform>();
            scrollBgButtonA.name = "Button " + i.ToString() + " left";
            scrollBgButtonA.SetParent(transform, false);

            scrollBgButtonA.anchorMin = new Vector2(0.5f, 0f);
            scrollBgButtonA.anchorMax = scrollBgButtonA.anchorMin;

            RectTransform scrollBgButtonB = Instantiate(scrollBgButtonA);
            scrollBgButtonB.name = "Button " + i.ToString() + " right";
            Button buttonBInstance = scrollBgButtonB.GetComponent<Button>();
            scrollBgButtonB.SetParent(transform, false);
            scrollBgButtonB.rotation = Quaternion.Euler(Vector3.forward * 180f);
            scrollBgButtonA.anchoredPosition3D = buttonApos;
            scrollBgButtonB.anchoredPosition3D = buttonBpos;

            buttonAInstance.onClick.AddListener(leftButton[i]);
            buttonBInstance.onClick.AddListener(rightButton[i]);
        }

        topImgDisp = new GameObject("TopImgDisp").AddComponent<Image>().GetComponent<RectTransform>();
        topImgDisp.SetParent(transform,false);
        topImgDisp.anchoredPosition3D = new Vector3(screenSize.x / 4f, screenSize.y - (screenSize.y / 4f) * 3f, 0f);

        midImgDisp = new GameObject("MidImgDisp").AddComponent<Image>().GetComponent<RectTransform>();
        midImgDisp.SetParent(transform, false);
        midImgDisp.anchoredPosition3D = new Vector3(screenSize.x / 4f, screenSize.y - (screenSize.y / 4f) * 4f, 0f);

        botImgDisp = new GameObject("BotImgDisp").AddComponent<Image>().GetComponent<RectTransform>();
        botImgDisp.SetParent(transform, false);
        botImgDisp.anchoredPosition3D = new Vector3(screenSize.x / 4f, screenSize.y - (screenSize.y / 4f) * 5f, 0f);

    }


    IEnumerator chooseTopObject(int direction)
    {

        int listLenght = topObjList.Count;
        Image imgComponent = topImgDisp.GetComponent<Image>();

        switch (direction)
        {
            case 0:
                topPos--;
                if (topPos < 0)
                    topPos = listLenght - 1;
                imgComponent.color = topObjList[topPos];
                //Debug.Log(topObjList[topPos].ToString());
                break;
            case 1:
                topPos++;
                if (topPos >= listLenght)
                    topPos = 0;
                imgComponent.color = topObjList[topPos];
                //Debug.Log(topObjList[topPos].ToString());
                break;
            default:
                Debug.Log("Error");
                break;
        }


        yield return 0;
    }

    IEnumerator chooseMidObject(int direction)
    {

        int listLenght = midObjList.Count;
        Image imgComponent = midImgDisp.GetComponent<Image>();

        switch (direction)
        {
            case 0:
                midPos--;
                if (midPos < 0)
                    midPos = listLenght - 1;
                imgComponent.color = midObjList[midPos];
                break;
            case 1:
                midPos++;
                if (midPos >= listLenght)
                    midPos = 0;
                imgComponent.color = midObjList[midPos];
                break;
            default:
                Debug.Log("Error");
                break;
        }


        yield return 0;
    }

    IEnumerator chooseBotObject(int direction)
    {
        int listLenght = botObjList.Count;
        Image imgComponent = botImgDisp.GetComponent<Image>();

        switch (direction)
        {
            case 0:
                botPos--;
                if (botPos < 0)
                    botPos = listLenght - 1;
                imgComponent.color = botObjList[botPos];
                break;
            case 1:
                botPos++;
                if (botPos >= listLenght)
                    botPos = 0;
                imgComponent.color = botObjList[botPos];
                break;
            default:
                Debug.Log("Error");
                break;
        }


        yield return 0;
    }

}
