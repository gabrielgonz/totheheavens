﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpikeTrapScript : MonoBehaviour {

    public List<Transform> spikes;

    private float startPosX;
    private float endPosX;
    private float lerpTime;
    private float currentLerpTime;

    // Use this for initialization
    void Start ()
    {
        startPosX = -7.5f;
        endPosX = 9f;
        lerpTime = 1;
        currentLerpTime = 0f;
        StartCoroutine(moveSpikes());
	}
	
    IEnumerator moveSpikes()
    {
        while(true)
        {
            currentLerpTime += Time.deltaTime;
            if(currentLerpTime>lerpTime)
            {
                currentLerpTime = 0f;
                float save = startPosX;
                startPosX = endPosX;
                endPosX = save;
            }

            float perc = currentLerpTime / lerpTime;

            float xMovement = Mathf.Lerp(startPosX, endPosX, perc);
            foreach (Transform item in spikes)
            {
                Vector3 pos = item.localPosition;
                pos.x = xMovement;
                item.localPosition = pos;
            }

            yield return 0;
        }
    }
}
