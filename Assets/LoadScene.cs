﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadScene : MonoBehaviour {

    public Text loadingText;

    public void Load()
    {
        StartCoroutine(LoadAsync());
    }

    IEnumerator LoadAsync()
    {
        loadingText.gameObject.SetActive(true);

        AsyncOperation sceneLoad = SceneManager.LoadSceneAsync(1);
        sceneLoad.allowSceneActivation = false;

        string text = loadingText.text;

        int counter = 0;
        while (sceneLoad.progress<.9f)
        {
            if (counter == 3)
            {
                counter = 0;
                loadingText.text = text;
            }

            loadingText.text = loadingText.text + '.';
            counter++;
            yield return 0;
        }

        sceneLoad.allowSceneActivation = true;

    }

}
